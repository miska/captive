#!/bin/sh
if [ "$REQUEST_METHOD" = "POST" ] && [ "`cat`" = "agree=on" ]; then
    captivectl add "$REMOTE_ADDR" > /dev/null 2>&1
    echo "Location: $HTTP_REFERER"
    echo
    cat << EOF
<html>
  <head>
    <meta http-equiv="refresh" content="3;url=$HTTP_REFERER"/>
  </head>
  <body>
    <a href="$HTTP_REFERER">Continue...</a>
    <script type="text/javascript">
      window.setTimeout(function(){
        window.location.replace("$HTTP_REFERER");
      }, 3000);
    </script>
  </body>
</html>
EOF
else
    # Empty headers
    echo
    LANG="`echo "$HTTP_ACCEPT_LANGUAGE" | sed -n 's|^\([a-z][a-z]\).*|\1|p'`"
    if [ -f "$DOCUMENT_ROOT"/form_${LANG}.sh ]; then
        "$DOCUMENT_ROOT"/form_${LANG}.sh
    elif [ -f "$DOCUMENT_ROOT"/form_${LANG}.html ]; then
        cat "$DOCUMENT_ROOT"/form_${LANG}.html
    elif [ -f "$DOCUMENT_ROOT"/form_default.sh ]; then
        "$DOCUMENT_ROOT"/form_default.sh
    else
        cat "$DOCUMENT_ROOT"/form_default.html
    fi
fi
